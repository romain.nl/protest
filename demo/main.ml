(* Run [make] to compile, and [_build/default/demo/main.exe] to run.

   If you just run [main.exe], tests are run assuming no previous tests are available.
   Run [main.exe -o results] to output test results in file [results].
   Run [main.exe -i results] to run tests and compare them with input file [results].
   Run [main.exe -i results -o results] to both compare with previous results and update them. *)

let () =
  Protest.main @@ fun session ->
  let test = Protest.test session in
  let get = Protest.get session in

  (* Run the shell command "echo hello everyone".
     The results of this command are "hello everyone" on stdout, "" on stderr
     and 0 as the exit code. *)
  test "echo" [ "hello everyone" ];

  (* Absence of a newline character is displayed using a black on white % symbol, like in zsh. *)
  test "echo" [ "-n"; "no new line here" ];

  (* Assuming you do not have a program named "anrsiutedlvladierst" in your PATH,
     the result of this command has exit code 127.
     If you then add this command to your PATH, Protest will tell you
     that the exit code changed. *)
  test "anrsiutedlvladierst" [];

  (* We will now run the "date" command, whose output changes regularly.
     Because this output changes, we use a regular expression to replace it with
     a fixed string and store it in a variable for later use.

     By setting [~mandatory] to [true], we tell Protest that if the pattern is not found,
     it should emit an error. *)
  let date_var =
    Protest.var ~constant: true ~mandatory: true ~name: "DATE"
      (Re.Perl.compile_pat "... \\d\\d ... \\d{4} \\d\\d:\\d\\d:\\d\\d ..")
  in
  test "date" [] ~output_vars: [ date_var ];

  (* We now use the DATE variable obtained from the previous command
     as the input of another command. Because this makes the command itself
     change, Protest would think that the test changed and would ask us if it
     is a new test. So we force the [title] to a fixed string (titles
     are what Protest use to compare commands).

     Note that this command also has a variable output.
     We reuse the DATE variable to fix the result.
     Note that we had set the [~constant] flag on this variable,
     so Protest will also check that its value is the same as before. *)
  test "echo" [ "date was"; get "DATE"; "CET" ] ~title: "echo date back"
    ~output_vars: [ date_var ];

  (* Here is an example where the output spans multiple lines.
     Add, remove or modify some lines to test the built-in diff algorithm
     (which is very not state-of-the-art).
     The fixed [~title] ensures that even if you change the lines to echo,
     Protest will not consider it a different test and thus will show you
     the diff in the output. *)
  test "echo" [ "hello everyone\nsomething else\nand another" ] ~title: "echo something";
  ()
