(** Process-based test framework. *)

(** Test results are written to file using a text format which is suitable for
    [diff]ing. Versioning this file by adding it to your project repository
    is thus a good idea. *)

(** Outputs of a test. *)
type results =
  {
    title: string;
    output: string;
    error: string;
    exit_code: int;
  }

(** Run external processes to get its results.

    Usage: [run command arguments]

    [title] is the name of the test.
    It is displayed when running the test, and it is used to decide whether
    two tests are the same. By default, it is built by concatenating quoted versions
    of [command] and [arguments]. If your command contains variable values,
    you should abstract them by giving an explicit [title] so that the test title
    does not depend on the specific value of those variables. Otherwise Protest
    would not be able to tell that this test is the same as the previous run.

    [output_vars] is the list of variables to search for in standard output.
    [error_vars] is the list of variables to search for in standard error.

    [env] can be used to set the environment variables of the new process.
    By default, it is the same as the current environment.

    [argv0] can be used to set the first item of the argument vector ([argv])
    of the new process. By default, it is [command]. *)
val run:
  ?title: string ->
  ?env: (string * string) list ->
  ?argv0: string ->
  string -> string list -> results

(** Test sessions. *)
type session

(** Start a test session.

    Read previous test results from [input] file if specified. *)
val start: ?input: string -> unit -> session

(** Finish a test session.

    Write test results to [output] file if specified. *)
val finish: ?output: string -> session -> unit

(** Add a test result to a test session and compare them with previous results. *)
val add: session -> results -> unit

(** {2 Variables} *)

(** Variables to extract from patterns in test outputs.

    [pattern] is matched on test output strings.
    If not found but [mandatory] is [true], the test fails.
    If found and variable was unbound, set variable to the value of the
    first captured group of [pattern].

    If pattern was found but variable was already bound,
    and if the new value is different, replace variable value if [constant] is [false];
    otherwise, fail.

    Variable values in standard output and standard error are replaced by [replacement],
    so that standard output and standard error can be compared with previous runs:
    a variable changing does not cause a test result to be considered different. *)
type variable_definition =
  {
    mandatory: bool;
    constant: bool;
    name: string;
    replacement: string;
    pattern: Re.re;
  }

(** Make a variable definition.

    Default value for [mandatory] and [constant] is [false].
    Default value for [name] is [VAR] followed by a unique number.
    Default value for [replacement] is ["%" ^ name ^ "%"]. *)
val var:
  ?mandatory: bool ->
  ?constant: bool ->
  ?name: string ->
  ?replacement: string ->
  Re.re -> variable_definition

(** Set the value of a variable.

    Usage: [set session name value]

    Usually variables are set by extracting patterns from process outputs though. *)
val set: session -> string -> string -> unit

(** Get the current value of a variable. *)
val get_opt: session -> string -> string option

(** Same as [get_opt], but interactively ask for a value if the variable is unbound. *)
val get: session -> string -> string

(** Substitute patterns by variable names (see {!variable_definition}). *)
val fix: session -> variable_definition list -> string -> string

(** {2 High-Level Functions} *)

(** Print and run an external command, substitute some variables and add results. *)
val test:
  session ->
  ?title: string ->
  ?output_vars: variable_definition list ->
  ?error_vars: variable_definition list ->
  ?env: (string * string) list ->
  ?argv0: string ->
  string -> string list -> unit

(** Read command line arguments, start a test session, run tests and finish the session.

    Command-line arguments are used to specify the [input] and [output] parameters
    of {!start} and {!finish}. *)
val main: (session -> unit) -> unit
