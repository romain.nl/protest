let echo x = Printf.ksprintf print_endline x
let error x = Printf.ksprintf (fun s -> prerr_endline s; exit 1) x

let reset = "\027[0m"
let bold = "\027[1m"
let reverse = "\027[7m"
let red = "\027[31m"
let green = "\027[32m"
let yellow = "\027[33m"
let blue = "\027[34m"
let magenta = "\027[35m"
let cyan = "\027[36m"

let prompt text =
  print_string yellow;
  print_string text;
  print_string reset;
  read_line ()

let ask prompt_text options =
  let prompt_text =
    let options = String.concat "|" (List.map (fun (char, _, _) -> char) options) in
    prompt_text ^ " [" ^ options ^ "|q|?] "
  in
  let is_default char = String.uppercase_ascii char = char in
  let rec loop () =
    let choice = String.lowercase_ascii (prompt prompt_text) in
    if choice = "q" then
      exit 1
    else
      match
        if choice = "" then
          List.find_opt (fun (char, _, _) -> is_default char) options
        else
          List.find_opt (fun (char, _, _) -> String.lowercase_ascii char = choice) options
      with
        | None ->
            (* Invalid choice, ask again. *)
            let output_option (char, title, _) =
              echo "%s: %s%s" char title (if is_default char then " (default)" else "")
            in
            List.iter output_option options;
            echo "q: quit without saving results";
            loop ()
        | Some (_, _, f) ->
            f ()
  in
  loop ()

let echo_output output =
  let len = String.length output in
  if len > 0 then (
    print_string output;
    if output.[len - 1] <> '\n' then echo "%s%%%s" reverse reset
  )

let split_lines string =
  match String.split_on_char '\n' string with
    | [ "" ] -> [] (* String.concat "\n" [] = String.concat "\n" [ "" ] *)
    | lines -> lines

(* TODO: fix how we display last new lines *)
let echo_diff before after =
  (* TODO: actual diff algorithm *)
  let rec diff a b =
    match a, b with
      | [], [] ->
          ()
      | [], _ :: _ ->
          List.iter (fun line -> echo "%s+ %s%s" green line reset) b
      | _ :: _, [] ->
          List.iter (fun line -> echo "%s- %s%s" red line reset) a
      | a_head :: a_tail, b_head :: b_tail ->
          if a_head = b_head then
            (
              echo "  %s" a_head;
              diff a_tail b_tail;
            )
          else
            let a_tail, b_tail, common_tail = diff_tail [] (List.rev a) (List.rev b) in
            List.iter (fun line -> echo "%s- %s%s" red line reset) a_tail;
            List.iter (fun line -> echo "%s+ %s%s" green line reset) b_tail;
            List.iter (echo "  %s") common_tail
  and diff_tail common_acc a b =
    match a, b with
      | a_head :: a_tail, b_head :: b_tail when a_head = b_head ->
          diff_tail (a_head :: common_acc) a_tail b_tail
      | _, _ ->
          List.rev a, List.rev b, common_acc
  in
  diff (split_lines before) (split_lines after)

type results =
  {
    title: string;
    output: string;
    error: string;
    exit_code: int;
  }

let shell_quote s =
  let needs_quotes = ref false in
  let only_single_quotes = ref false in
  for i = 0 to String.length s - 1 do
    match s.[i] with
      | 'a'..'z' | 'A'..'Z' | '0'..'9' | '.' | '-' | '_' | ':' | '+' | '%' | '^' | '/' ->
          ()
      | '\'' ->
          needs_quotes := true
      | _ ->
          needs_quotes := true;
          only_single_quotes := false
  done;
  if !needs_quotes then
    if !only_single_quotes then
      String.split_on_char '\'' s |> String.concat "\\'"
    else
      "'" ^ (String.split_on_char '\'' s |> String.concat "'\"'\"'") ^ "'"
  else
    s

let make_title title command arguments =
  match title with
    | None ->
        String.concat " " (List.map shell_quote (command :: arguments))
    | Some title ->
        title

let run ?title ?env ?argv0 command arguments =
  (* Spawn process. *)
  let argv0 = match argv0 with None -> command | Some x -> x in
  let args = Array.of_list (argv0 :: arguments) in
  let stdout_exit, stdout_entrance = Unix.pipe () in
  Unix.set_close_on_exec stdout_exit;
  let stderr_exit, stderr_entrance = Unix.pipe () in
  Unix.set_close_on_exec stderr_exit;
  let pid =
    match env with
      | None ->
          Unix.create_process command args Unix.stdin
            stdout_entrance stderr_entrance
      | Some env ->
          let env =
            List.map (fun (name, value) -> name ^ "=" ^ value) env
            |> Array.of_list
          in
          Unix.create_process_env command args env Unix.stdin
            stdout_entrance stderr_entrance
  in
  Unix.close stdout_entrance;
  Unix.close stderr_entrance;

  (* Read all process output. *)
  let chunk_size = 256 in
  let output_buf = Buffer.create chunk_size in
  let error_buf = Buffer.create chunk_size in
  let bytes = Bytes.create chunk_size in
  let read_chunk fd buf =
    let len = Unix.read fd bytes 0 chunk_size in
    Buffer.add_subbytes buf bytes 0 len;
    len = 0
  in
  let rec loop output_done error_done =
    if not (output_done && error_done) then
      let pending_reads_1 = if output_done then [] else [ stdout_exit ] in
      let pending_reads_2 = if error_done then [] else [ stderr_exit ] in
      match Unix.select (pending_reads_1 @ pending_reads_2) [] [] (-1.) with
        | exception Unix.Unix_error (EINTR, _, _) ->
            loop output_done error_done
        | ready, _, _ ->
            let output_done =
              if List.mem stdout_exit ready then
                read_chunk stdout_exit output_buf
              else
                output_done
            in
            let error_done =
              if List.mem stderr_exit ready then
                read_chunk stderr_exit error_buf
              else
                error_done
            in
            loop output_done error_done
  in
  loop false false;
  let output = Buffer.contents output_buf in
  Buffer.reset output_buf;
  let error_output = Buffer.contents error_buf in
  Buffer.reset error_buf;
  let exit_code =
    let _, status = Unix.waitpid [] pid in
    match status with
      | WEXITED code ->
          code
      | WSIGNALED code ->
          error "process was killed by signal %d" code
      | WSTOPPED code ->
          error "process was stopped by signal %d" code
  in

  (* Return results. *)
  {
    title = make_title title command arguments;
    output;
    error = error_output;
    exit_code;
  }

module String_map = Map.Make (String)

type session =
  {
    mutable previous_results: results list;
    mutable current_results: results list; (* in reverse order *)
    mutable variables: string String_map.t;
  }

let with_open_in filename f =
  let ch = open_in filename in
  match f ch with
    | exception exn ->
        close_in ch;
        raise exn
    | x ->
        close_in ch;
        x

let start ?input () =
  let previous_results =
    match input with
      | None ->
          []
      | Some filename ->
          with_open_in filename @@ fun ch ->
          let input_number () =
            match input_line ch with
              | exception End_of_file | "" ->
                  None
              | line ->
                  match int_of_string_opt line with
                    | None ->
                        error "invalid integer in input file: %S" line
                    | Some n ->
                        Some n
          in
          let input_multiline () =
            match input_number () with
              | None ->
                  None
              | Some count ->
                  let rec loop acc count =
                    if count > 0 then
                      let line =
                        match input_line ch with
                          | exception End_of_file ->
                              error "input file ends unexpectedly"
                          | line ->
                              line
                      in
                      loop (line :: acc) (count - 1)
                    else
                      String.concat "\n" (List.rev acc)
                  in
                  Some (loop [] count)
          in
          let rec loop acc =
            match input_multiline () with
              | None ->
                  List.rev acc
              | Some title ->
                  let output =
                    match input_multiline () with
                      | None ->
                          error "command with no output in input file: %S" title
                      | Some output ->
                          output
                  in
                  let error_output =
                    match input_multiline () with
                      | None ->
                          error "command with no error output in input file: %S" title
                      | Some error_output ->
                          error_output
                  in
                  let exit_code =
                    match input_number () with
                      | None ->
                          error "command with no exit code in input file: %S" title
                      | Some exit_code ->
                          exit_code
                  in
                  let results =
                    {
                      title;
                      output;
                      error = error_output;
                      exit_code;
                    }
                  in
                  loop (results :: acc)
          in
          loop []
  in
  {
    previous_results;
    current_results = [];
    variables = String_map.empty;
  }

let with_open_out filename f =
  let ch = open_out filename in
  match f ch with
    | exception exn ->
        close_out ch;
        raise exn
    | x ->
        close_out ch;
        x

let finish ?output session =
  match output with
    | None ->
        ()
    | Some filename ->
        with_open_out filename @@ fun ch ->
        let final_results = List.rev session.current_results in
        let output_line line =
          output_string ch line;
          output_char ch '\n'
        in
        let output_number n = output_line (string_of_int n) in
        let output_multiline string =
          let lines = split_lines string in
          output_number (List.length lines);
          List.iter output_line lines;
        in
        let output_results results =
          output_multiline results.title;
          output_multiline results.output;
          output_multiline results.error;
          output_number results.exit_code
        in
        List.iter output_results final_results

let rec add session results =
  let add_as_new () =
    session.current_results <- results :: session.current_results
  in
  let echo_results () =
    echo_output results.output;
    if results.error <> "" then (
      echo "%sError output:%s" magenta reset;
      echo_output results.error;
    );
    if results.exit_code <> 0 then
      echo "%sExit code:%s %d" magenta reset results.exit_code
  in
  match session.previous_results with
    | [] ->
        echo_results ();
        ask "This is a new test." [
          "A", "add as a new test", add_as_new;
        ]
    | head :: tail ->
        let delete_previous () =
          echo "%s%s%s%s (still)" bold blue results.title reset;
          session.previous_results <- tail;
          add session results
        in
        let replace_previous () =
          session.previous_results <- tail;
          session.current_results <- results :: session.current_results
        in
        if results.title <> head.title then
          (
            echo_results ();
            echo "%sPrevious test was:" yellow;
            (* TODO: highlight diff *)
            echo "%s%s%s%s" bold cyan head.title reset;
            ask "Test title differs." [
              "A", "add as a new test", add_as_new;
              "d", "delete previous test", delete_previous;
              "r", "replace previous test", replace_previous;
            ]
          )
        else
          let equal = ref true in
          if results.output <> head.output then (
            echo_diff head.output results.output;
            equal := false
          );
          if results.error <> head.error then (
            echo_diff head.error results.error;
            equal := false
          );
          if results.exit_code <> head.exit_code then (
            echo "%sExit code:%s %s%d%s (expected %s%d%s)" magenta reset
              green results.exit_code reset red head.exit_code reset;
            equal := false
          );
          if !equal then
            replace_previous ()
          else
            ask "Test result differs." [
              "a", "add as a new test", add_as_new;
              "d", "delete previous test", delete_previous;
              "R", "replace previous test", replace_previous;
            ]

type variable_definition =
  {
    mandatory: bool;
    constant: bool;
    name: string;
    replacement: string;
    pattern: Re.re;
  }

let var =
  let next = ref 0 in
  fun ?(mandatory = false) ?(constant = false) ?name ?replacement pattern ->
    let name =
      match name with
        | None ->
            let index = !next in
            incr next;
            "VAR" ^ string_of_int index
        | Some name ->
            name
    in
    let replacement =
      match replacement with
        | None ->
            "%" ^ name ^ "%"
        | Some replacement ->
            replacement
    in
    {
      mandatory;
      constant;
      name;
      replacement;
      pattern;
    }

let set session name value =
  session.variables <- String_map.add name value session.variables

let get_opt session name =
  String_map.find_opt name session.variables

let get session name =
  match get_opt session name with
    | None ->
        let value = prompt "Enter value for variable %s: " in
        set session name value;
        value
    | Some value ->
        value

let fix_one session variable string =
  match Re.exec_opt variable.pattern string with
    | None ->
        if variable.mandatory then
          error "pattern not found for variable %s" variable.name;
        string
    | Some groups ->
        let value, group_index =
          try
            Re.Group.get groups 1, 1
          with Not_found ->
            Re.Group.get groups 0, 0
        in
        if variable.constant then (
          match get_opt session variable.name with
            | None ->
                ()
            | Some old_value ->
                if value <> old_value then
                  error "variable %s has value %S, but here has different value %S"
                    variable.name old_value value
        );
        set session variable.name value;
        (* Cannot use Re.replace because it only replaces group 0 (i.e. the full match). *)
        let ofs, ofs_end = Re.Group.offset groups group_index in
        String.concat "" [
          String.sub string 0 ofs;
          variable.replacement;
          String.sub string ofs_end (String.length string - ofs_end);
        ]

let fix session variables string =
  List.fold_left (fun acc var -> fix_one session var acc) string variables

let test session ?title ?(output_vars = []) ?(error_vars = []) ?env ?argv0
    command arguments =
  let title = make_title title command arguments in
  echo "%s%s%s%s" bold blue title reset;
  let results = run ~title ?env ?argv0 command arguments in
  let output = fix session output_vars results.output in
  let error = fix session error_vars results.error in
  add session {
    title = results.title;
    output;
    error;
    exit_code = results.exit_code;
  }

let parse_argv () =
  let input = ref "" in
  let output = ref "" in
  let spec =
    Arg.align [
      "-i", Set_string input, "<FILENAME> Compare with previous results from FILENAME.";
      "-o", Set_string output, "<FILENAME> Store results in FILENAME.";
    ]
  in
  Arg.parse spec (fun x -> raise (Arg.Bad ("don't know what to do with: " ^ x)))
    ("Usage: " ^ Sys.executable_name ^ " [OPTIONS]");
  let input = if !input = "" then None else Some !input in
  let output = if !output = "" then None else Some !output in
  input, output

let main f =
  let input, output = parse_argv () in
  let session = start ?input () in
  f session;
  finish ?output session
