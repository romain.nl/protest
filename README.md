# Protest

A library to perform tests without having to describe what outputs are expected.

This is a proof of concept and the interface cannot be considered to be stable.

## Philosophy

The idea is to write calls, and only calls.
Outputs can be stored in a text file which can be added to your repository
for versioning. Future test runs can be compared with previous results
read from this text file. If results differ, Protest interactively asks you
what to do (typically, replace previous results, add as a new test, or quit).

Sometimes, your calls may return non-deterministic results, such as random
hashes or dates. Because those results are (almost) never the same between
two test runs, they make the tests fail. To fix this, you may specify regular
expressions to find in outputs and to replace with fixed strings.

Sometimes, the non-deterministic parts of results need to be given as inputs
to other tests. Protest can store the values matched by the regular expressions
you specify in variables. You can then read those variables and use them in
future tests.

This approaches works in particular well for tests which consists in running
command-line commands. But you can also use it for any function as long as
you have a way to convert their output into strings.

## Documentation

See `src/protest.mli` for documentation and `demo/main.ml` for an example.

## Compile

You need the `re` library for regular expressions:

    opam install re

Compile using `make`.
